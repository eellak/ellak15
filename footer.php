<?php
/**
 * The template for displaying the footer.
 *
 * @package Brandon
 * @author Muffin group
 * @link http://muffingroup.com
 */
?>

</div>

<!-- #Footer -->		
<footer id="Footer" class="clearfix">

	<div class="footer_line">
		<div class="container">
			<div class="line line_1"></div>
			<div class="line line_2"></div>
			<div class="line line_3"></div>
			<div class="line line_4"></div>
		</div>
	</div>

	<div class="widgets_wrapper">
		<div class="container">
			<?php
				$sidebars_count = 0;	
				for( $i = 1; $i <= 4; $i++ ){
					if ( is_active_sidebar( 'footer-area-'. $i ) ) $sidebars_count++;
				}
			
				$sidebar_class = '';
				if( $sidebars_count > 0 ){
					switch( $sidebars_count ){
						case 2: $sidebar_class = 'one-second'; break; 
						case 3: $sidebar_class = 'one-third'; break; 
						case 4: $sidebar_class = 'one-fourth'; break;
						default: $sidebar_class = 'one';
					}
				}
			?>
			
			<?php 
				for( $i = 1; $i <= 4; $i++ ){
					if ( is_active_sidebar( 'footer-area-'. $i ) ){
						echo '<div class="'. $sidebar_class .' column">';
							dynamic_sidebar( 'footer-area-'. $i );
						echo '</div>';
					}
				}
			?>
	
		</div>
	</div>

	<div class="footer_extra">
		<a href="<?php echo site_url( '/wikis/%CF%87%CF%8E%CF%81%CE%BF%CF%82-%CF%83%CF%85%CE%BD%CE%B5%CF%81%CE%B3%CE%B1%CF%83%CE%AF%CE%B1%CF%82/%CE%BF%CE%BC%CE%AC%CE%B4%CE%B5%CF%82-%CE%B5%CF%81%CE%B3%CE%B1%CF%83%CE%AF%CE%B5%CF%82/' ); ?>"><?php _e( 'View all GFOSS Workteams', 'ellak15' ); ?></a>
	</div>

	<div class="footer_menu">
		<div class="container">
		
			<?php mfn_wp_footer_menu(); ?>
			
			<div class="copyright">
				<?php 
					if( mfn_opts_get('footer-copy') ){
						mfn_opts_show('footer-copy');
					} else {
						echo '&copy; '. date( 'Y' ) .' '. get_bloginfo( 'name' ) .'. All Rights Reserved. <a target="_blank" rel="nofollow" href="http://muffingroup.com">Muffin group</a>';
					}
				?>
			</div>
			
		</div>
	</div>
	
	<a id="back_to_top" href="#"><span></span></a>
	
</footer>
	
<!-- wp_footer() -->
<?php wp_footer(); ?>

</body>
</html>
