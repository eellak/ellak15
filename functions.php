<?php

// load parent & child stylesheets
function ellak15_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri()
		. '/style.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri()
		. '/style.css', array('parent-style')
	);
	wp_enqueue_script( 'facebook-sdk', get_stylesheet_directory_uri() . '/js/facebook.js', array(), '2.3', true );
}
add_action( 'wp_enqueue_scripts', 'ellak15_enqueue_styles' );

/* theme setup */
function ellak15_setup() {
	/* child theme translations in /languages/ */
	load_child_theme_textdomain( 'ellak15', get_stylesheet_directory()
		. '/languages' );

	/* hide admin bar for subscribers */
	$user = wp_get_current_user();
	if( in_array( 'subscriber', $user->roles ) ) {
		show_admin_bar( false );
	}
}
add_action( 'after_setup_theme', 'ellak15_setup' );

?>
